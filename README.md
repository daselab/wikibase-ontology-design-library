# README

## Abbreviated Patterns

#### abbreviatedPatternsV1
![The Abbreviated Patterns (V1)](/diagrams/abbreviatedPatterns/abbreviatedPatternsV1.jpg)
#### abbreviatedPatternsV2
![The Abbreviated Patterns (V2)](/diagrams/abbreviatedPatterns/abbreviatedPatternsV2.jpg)



## Expanded Patterns

#### expandedPatternsV1
![The Expanded Patterns (V1)](/diagrams/expandedPatterns/expandedPatternsV1.jpg)
#### expandedPatternsV2
![The Expanded Patterns (V2)](/diagrams/expandedPatterns/expandedPatternsV2.jpg)

## KS Agricultural
##### ksAgriculturalCollege_abbreviatedWithValues
![KS Agricultural College (Abbreviated With Values)](/diagrams/ksAgricultrualCollege_abbreviatedWithValues/ksAgriculturalCollege_abbreviatedWithValues.jpg)

##### ksAgriculturalCollege_abbreviatedNoValues
![KS Agricultural College (Abbreviated No Values)](/diagrams/ksAgriculturalCollege_abbreviatedNoValues/ksAgriculturalCollege_abbreviatedNoValues.jpg)

##### ksAgriculturalCollege_expandedWithValues
![KS Agricultural College (Expanded With Values)](/diagrams/ksAgriculturalCollege_expandedWithValues/ksAgriculturalCollege_expandedWithValues.jpg)


##### ksAgriculturalCollege_expandedNoValues
![KS Agricultural College (Expanded No Values)](/diagrams/ksAgriculturalCollege_expandedNoValues/ksAgriculturalCollege_expandedNoValues.jpg)



## General Patterns

##### qualifier-datetime
![Qualifier-Datetime Pattern](/diagrams/qualifier-datetime/qualifier-datetime.jpg)

##### qualifier-string
![Qualifier-String Pattern](/diagrams/qualifier-string/qualifier-string.jpg)

##### quantityAsQualifier
![Quantity As Qualifier Pattern](/diagrams/quantityAsQualifier/quantityAsQualifier.jpg)

##### ref-without-qualifier
![Reference Without Qualifier Pattern](/diagrams/ref-without-qualifier/ref-without-qualifier.jpg)


##### statement-datetime
![Statement-Datetime Pattern](/diagrams/statement-datetime/statement-datetime.jpg)


##### statement-string
![Statement-String Pattern](/diagrams/statement-string/statement-string.jpg)

##### wd-general
![General Wikidata Pattern](/diagrams/wd-general/wd-general.jpg)
