#shapes Wikibase Patterns
#statement a date

PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>
PREFIX xsd: <http://www.w3.org/2001/XMLSchema#>
PREFIX ontolex: <http://www.w3.org/ns/lemon/ontolex#>
PREFIX dct: <http://purl.org/dc/terms/>
PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>
PREFIX owl: <http://www.w3.org/2002/07/owl#>
PREFIX wikibase: <http://wikiba.se/ontology#>
PREFIX skos: <http://www.w3.org/2004/02/skos/core#>
PREFIX schema: <http://schema.org/>
PREFIX cc: <http://creativecommons.org/ns#>
PREFIX geo: <http://www.opengis.net/ont/geosparql#>
PREFIX prov: <http://www.w3.org/ns/prov#>
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX data: <https://www.wikidata.org/wiki/Special:EntityData/>
PREFIX s: <http://www.wikidata.org/entity/statement/>
PREFIX ref: <http://www.wikidata.org/reference/>
PREFIX v: <http://www.wikidata.org/value/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>
PREFIX wdtn: <http://www.wikidata.org/prop/direct-normalized/>
PREFIX p: <http://www.wikidata.org/prop/>
PREFIX ps: <http://www.wikidata.org/prop/statement/>
PREFIX psv: <http://www.wikidata.org/prop/statement/value/>
PREFIX psn: <http://www.wikidata.org/prop/statement/value-normalized/>
PREFIX pq: <http://www.wikidata.org/prop/qualifier/>
PREFIX pqv: <http://www.wikidata.org/prop/qualifier/value/>
PREFIX pqn: <http://www.wikidata.org/prop/qualifier/value-normalized/>
PREFIX pr: <http://www.wikidata.org/prop/reference/>
PREFIX prv: <http://www.wikidata.org/prop/reference/value/>
PREFIX prn: <http://www.wikidata.org/prop/reference/value-normalized/>
PREFIX wdno: <http://www.wikidata.org/prop/novalue>


start=@<ItemType>

<ItemType> {

wdt:propertyName xsd:dateTime;

p:propertyName { 
    rdf:type wikibase:Statement;
    ps:propertyName @<#itemDateTime>+; 
    pq:qualifierName @<#qualifier>*;
    pq:qualifierName @<#qualifierDateTime>*;
    pq:qualifierName @<#qualifierQuantity>*;
    pq:qualifierName @<#qualifierString>*
    prov:wasDerivedFrom @<#reference>*;
    prov:wasDerivedFrom @<#referenceDateTime>*;
    prov:wasDerivedFrom @<#referenceQuantity>*;
    prov:wasDerivedFrom @<#referenceString>*;
   
}+;
<#qualifierDateTime> {
    rdf:type wikibase:Statement;
    pq:qualifierName xsd:dateTime+;
    psv:qualifierName @<#precitionDate>;
 }+;

<#qualifier> {
  rdf:type wikibase:Statement; 
  pq:qualifierName wd:IRI; 
}*;

<#qualifierString> {
  rdf:type wikibase:Statement; 
  pq:qualifierName xsd:string; 
}*;

<#qualifierDateTime> {
  rdf:type wikibase:Statement;
  pq:qualifierName xsd:dateTime+;
  pqv:qualifierName @<#precitionDate>;
}*;

<#qualifierQuantity> {
  rdf:type wikibase:Statement;
  pq:qualifierName xsd:decimal+;
  pqv:qualifierName @<#precitionQuantity>;
}*;

#references
<#reference> {
  a wikibase:Reference;
  pr:referenceName IRI+;
}*;
<#referenceString> {
    a wikibase:Reference;
    pr:referenceName xsd:string; 
  }*;
  
<#referenceDateTime> {
    a wikibase:Reference;
    pr:referenceName xsd:dateTime+;
    prv:referenceName @<#precitionDate>; 
  }*;
  <#referenceQuantity> {
    a wikibase:Reference;
    pr:referenceName xsd:decimal+;
    prv:referenceName @<#precitionQuantity>;
  }*;

<#precitionQuantity> {
  rdf:type wikibase:QuantityValue;
  wikibase:quantityAmount xsd:decimal;
  wikibase:quantityUnit wd:IRI; 
}                                
<#precitionDate> {
  rdf:type wikibase:TimeValue; 
  wikibase:timeValue xsd:dateTime;
  wikibase:timePrecision xsd:int;
  wikibase:timeTimezone xsd:int;
  wikibase:timeCalendarModel wd:IRI; 
};